class Product < ApplicationRecord
  include PgSearch::Model

  mount_uploader :picture, PictureUploader

  pg_search_scope :search_for, against: %i(title description)

  validates :title, :price, presence: true

  validate :picture_size_validation

  def picture_size_validation
    errors[:picture] << "should be less than 100KB" if picture.size > 0.1.megabytes
  end
end
